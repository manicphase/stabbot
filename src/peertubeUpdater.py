from requests_oauthlib import OAuth2Session
from oauthlib.oauth2 import LegacyApplicationClient

class Updater:
    def __init__(self, secret):
        # this is just ripped out of the prismedia thing, but we pass the secret in
        # as a dict
        peertube_url = str(secret.get('peertube_url')).rstrip("/")

        oauth_client = LegacyApplicationClient(
            client_id=str(secret.get('client_id'))
        )

        oauth = OAuth2Session(client=oauth_client)
        oauth.fetch_token(
            token_url=str(peertube_url + '/api/v1/users/token'),
            # lower as peertube does not store uppercase for pseudo
            username=str(secret.get('username').lower()),
            password=str(secret.get('password')),
            client_id=str(secret.get('client_id')),
            client_secret=str(secret.get('client_secret'))
            )

        self.peertube_url = peertube_url
        self.client = oauth
        self.metadata = {}
        self.newdata = {}


    def updateVideo(self, video_id):
        """video_id: the uuid from a peertube video.

           https://docs.joinpeertube.org/api-rest-reference.html#tag/Video/paths/~1videos~1{id}/put
           Only included fields are updated, absent fields remain as they were.
        """
        video_url = self.peertube_url + "/api/v1/videos/" + video_id
        response = self.client.put(video_url, self.newdata[video_id])
        return response


    def getMetadata(self, video_id):
        # this just caches the metadata so we don't start spamming get requests
        # if we end up wanting to read/update multiple fields in future.
        if self.metadata.get(video_id):
            return self.metadata[video_id]
        else:
            video_url = self.peertube_url + "/api/v1/videos/" + video_id
            self.metadata[video_id] = self.client.get(video_url).json()
            return self.metadata[video_id]


    def addTag(self, video_id, tag):
        tags = self.getMetadata(video_id)["tags"]
        tags.append(tag)
        if not self.newdata.get(video_id):
            self.newdata[video_id] = {}
        self.newdata[video_id]["tags"] = tags


    def addSummonToDescription(self, video_id, url):
        description = self.getMetadata(video_id)["description"]
        desc_lines = description.split("\r\n")
        desc_lines.insert(2, "stabilization was also requested here: " + url)
        desc_lines.insert(3, "")
        description = "\r\n".join(desc_lines)
        if not self.newdata.get(video_id):
            self.newdata[video_id] = {}
        self.newdata[video_id]["description"] = description
